# frozen_string_literal: true

require 'pathname'
require 'yaml'
require 'license_finder'
require 'license/management/loggable'
require 'license/management/verifiable'
require 'license/management/repository'
require 'license/management/report'
require 'license/management/version'

# This applies a monkey patch to the JsonReport found in the `license_finder` gem.
LicenseFinder::JsonReport.prepend(License::Management::Report)

module License
  module Management
    def self.root
      Pathname.new(File.dirname(__FILE__)).join('../..')
    end
  end
end
