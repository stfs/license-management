# frozen_string_literal: true

module License
  module Management
    class Repository
      include Loggable
      include Verifiable
      KNOWN_SOURCES = [
        'licenses.nuget.org',
        'opensource.org',
        'www.opensource.org',
      ].freeze

      def initialize(
        compatibility_path: License::Management.root.join('normalized-licenses.yml'),
        spdx_path: License::Management.root.join('spdx-licenses.json')
      )
        @compatibility_data = YAML.safe_load(IO.read(compatibility_path))
        @spdx_data = load_spdx_data_from(spdx_path)
      end

      def item_for(license)
        spdx_data_for(id_for(license)) ||
          spdx_data_for(license.send(:short_name)) ||
          generate_item_for(license)
      end

      private

      attr_reader :spdx_data, :compatibility_data

      def spdx_data_for(id)
        return if blank?(id)

        data = spdx_data[id]
        if data
          {
            'id' => data['licenseId'],
            'name' => data['name'],
            'url' => data['seeAlso'][-1]
          }
        end
      end

      def id_for(license)
        ids = compatibility_data['ids']
        ids[license.send(:short_name)] ||
          ids[license.url] ||
          known_sources(license.send(:short_name)) ||
          known_sources(license.url)
      end

      # When `license_finder` is unable to determine the license it will use the full
      # content of the file as the name of the license. This method shrinks that name
      # down to just take the first line of the file.
      def take_first_line_from(content)
        return '' if blank?(content)

        content.split(/[\r\n]+/)[0]
      end

      def generate_item_for(license)
        log_info("detected unknown license named `#{license.send(:short_name)}`:`#{license.url}`")
        name = take_first_line_from(license.name)
        {
          'id' => name.downcase,
          'name' => name,
          'url' => present?(license.url) ? license.url : ''
        }
      end

      def load_spdx_data_from(path)
        content = IO.read(path)
        json = JSON.parse(content)
        licenses = json['licenses']

        licenses.inject({}) do |memo, license|
          memo[license['licenseId']] = license
          memo
        end
      end

      def known_sources(url)
        return if blank?(url)
        return unless url =~ /\A#{::URI::DEFAULT_PARSER.make_regexp(['http', 'https'])}\z/

        uri = URI.parse(url)
        return unless KNOWN_SOURCES.include?(uri.host.downcase)
        uri.path.split('/')[-1]
      rescue => error
        log_info(error)
        nil
      end
    end
  end
end
