# frozen_string_literal: true

require 'license/management/report/base'
require 'license/management/report/v1'
require 'license/management/report/v1_1'
require 'license/management/report/v2'

module License
  module Management
    module Report
      DEFAULT_VERSION = '1'
      VERSIONS = {
        nil => V1,
        '' => V1,
        '1' => V1,
        '1.0' => V1,
        '1.1' => V1_1,
        '2' => V2,
        '2.0' => V2
      }.freeze

      # This method overrides the method defined in `LicenseFinder::JsonReport` to
      # allow us to generate a custom json report.
      def to_s
        JSON.pretty_generate(version_for(report_version).to_h) + "\n"
      end

      private

      def report_version
        ENV.fetch('LM_REPORT_VERSION', DEFAULT_VERSION)
      end

      def version_for(version)
        VERSIONS.fetch(version.to_s).new(dependencies)
      end
    end
  end
end
