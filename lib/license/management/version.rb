# frozen_string_literal: true

module License
  module Management
    VERSION = '1.7.4'
  end
end
