# frozen_string_literal: true

module License
  module Management
    module Report
      class V2 < Base
        def to_h
          {
            version: '2.0',
            licenses: license_summary,
            dependencies: dependencies.sort_by(&:name).map { |x| map_from(x) }
          }
        end

        private

        def all_licenses
          dependencies.map { |x| x.licenses.to_a }.flatten
        end

        def license_summary
          all_licenses
            .group_by { |x| data_for(x)['name'] }
            .sort_by { |x, y| [-y.size, x] }
            .map { |_name, items| data_for(items[0]).merge(count: items.count) }
        end

        def data_for(license)
          repository.item_for(license)
        end

        def map_from(dependency)
          {
            name: dependency.name,
            url: dependency.homepage,
            description: description_for(dependency),
            paths: paths_from(dependency),
            licenses: dependency.licenses.map { |x| data_for(x)['id'] }
          }
        end
      end
    end
  end
end
