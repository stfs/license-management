# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'license/management/version'

Gem::Specification.new do |spec|
  spec.name          = 'license-management'
  spec.version       = License::Management::VERSION
  spec.authors       = ['Fabien Catteau', 'Olivier Gonzalez', 'mo khan']
  spec.email         = ['fcatteau@gitlab.com', 'ogonzalez@gitlab.com', 'mkhan@gitlab.com']

  spec.summary       = 'License Management job for GitLab CI.'
  spec.description   = 'License Management job for GitLab CI.'
  spec.homepage      = 'https://gitlab.com/gitlab-org/security-products/license-management'
  spec.license       = 'GitLab EE'

  spec.metadata['allowed_push_host'] = 'https://example.com'
  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/gitlab-org/security-products/license-management'
  spec.metadata['changelog_uri'] = 'https://gitlab.com/gitlab-org/security-products/license-management/blob/master/CHANGELOG.md'

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir.glob('exe/*') + Dir.glob('lib/**/**/*.{rb,yml}') + Dir.glob('*.{md,yml,json}')
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'license_finder', ENV.fetch('LICENSE_FINDER_VERSION', '5.6.2')
  spec.add_development_dependency 'rspec', '~> 3.9'
end
