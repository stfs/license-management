RSpec.describe License::Management::Repository do
  describe "#item_for" do
    let(:spdx_licenses) { JSON.parse(IO.read('spdx-licenses.json'))['licenses'] }

    context "when mapping a license that refers to opensource.org" do
      it 'parses the SPDX id from the url' do
        spdx_licenses.each do |license|
          spdx_id = license['licenseId']
          url = "https://opensource.org/licenses/#{spdx_id}"
          license = LicenseFinder::License.new(short_name: url, matcher: LicenseFinder::License::NoneMatcher.new, url: url)
          expect(subject.item_for(license)['id']).to eql(spdx_id)
        end
      end
    end

    context "when mapping a license that refers to nuget.org" do
      it 'parses the SPDX id from the url' do
        spdx_licenses.each do |license|
          spdx_id = license['licenseId']
          url = "https://licenses.nuget.org/#{spdx_id}"
          license = LicenseFinder::License.new(short_name: url, matcher: LicenseFinder::License::NoneMatcher.new, url: url)
          expect(subject.item_for(license)['id']).to eql(spdx_id)
        end
      end
    end
  end
end
