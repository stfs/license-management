# GitLab License Management

[![pipeline status](https://gitlab.com/gitlab-org/security-products/license-management/badges/master/pipeline.svg)](https://gitlab.com/gitlab-org/security-products/license-management/commits/master)
[![coverage report](https://gitlab.com/gitlab-org/security-products/license-management/badges/master/coverage.svg)](https://gitlab.com/gitlab-org/security-products/license-management/commits/master)

GitLab tool for detecting licenses of the dependencies used by the provided source.
It is currently based on [License Finder](https://gitlab.com/gitlab-org/security-products/license-management)
only, but this may change in the future.

## How to use

1. `cd` into the directory of the source code you want to scan
1. Run the Docker image:

    ```sh
    docker run \
      --volume "$PWD":/code \
      --rm \
      registry.gitlab.com/gitlab-org/security-products/license-management:latest analyze /code
    ```

1. The results will be stored in the `gl-license-management-report.json` file in the application directory. `gl-license-management-report.html` is also available with a human readable report.

## Development

### Running the application

License Management is a Docker image, you can build it like any Docker image like so in the
project root:

```sh
docker build -t license-management .
```

You can then run License Management on some target directory:

```sh
docker run --rm --volume "/path/to/my/project":/code license-management analyze /code
```

You can run integration tests on the image like this:

```sh
./bin/test_all
```

where:

- `project_name` is the project slug among the
[test projects for security products](https://gitlab.com/gitlab-org/security-products/tests/) (e.g. `java-maven`)
- `git_ref` is the Git ref to checkout and scan against (e.g. `master`)

### Updating the SPDX index

We will need to periodically update the SPDX index. This can be achieved with
the following command.

```bash
$ ./bin/update-spdx
```

## Supported languages and package managers

The following table shows which languages and package managers are supported.

| Language   | Package managers                                                  |
|------------|-------------------------------------------------------------------|
| JavaScript | [Bower](https://bower.io/), [npm](https://www.npmjs.com/)         |
| Go         | [Godep](https://github.com/tools/godep), go get                   |
| Java       | [Gradle](https://gradle.org/), [Maven](https://maven.apache.org/) |
| .NET       | [Nuget](https://www.nuget.org/)                                   |
| Python     | [pip](https://pip.pypa.io/en/stable/)                             |
| Ruby       | [gem](https://rubygems.org/)                                      |

Inject `SETUP_CMD` to the docker command to override the given package managers
and run your custom command to setup your environment with a custom package manager.

```sh
docker run \
  --volume "$PWD":/code \
  --env "SETUP_CMD=./my-custom-install-script.sh" \
  --rm \
  registry.gitlab.com/gitlab-org/security-products/license-management:latest analyze /code
```

## Settings

The License Management tool can be customized with environments variables for some project types. 

| Environment variable | Project type | Function |
|----------------------|--------------|----------|
| MAVEN_CLI_OPTS       | Java (Maven) | Additional arguments for the mvn executable. If not supplied, defaults to `-DskipTests`. |
| LM_JAVA_VERSION      | Java (Maven) | Version of Java. If set to `11`, Maven and Gradle use Java 11 instead of Java 8. |
| LM_PYTHON_VERSION    | Python       | Version of Python. If set to `3`, dependencies are installed using Python 3 instead of Python 2.7. |


Inject the required environment variables to the docker command using the [`--env` option flag](https://docs.docker.com/engine/reference/commandline/run/#set-environment-variables--e---env---env-file)
or its shorthand form (`--env MY_SETTING_VAR`) if the configuration comes from an external environment.


*Caution:* if you supply the `MAVEN_CLI_OPTS` for a Maven project, don't forget to append `-DskipTests` to save time by skipping the test suite.

*Tip:* in case you still need to run tests during license management check, supply `MAVEN_CLI_OPTS=-DskipTests=false`
to the docker command.


## Versioning and release process

1. Create a new entry in the `.gitlab-ci.yml` file for the new version to release.

    ```yaml
    12-x-stable:
      extends: .release
      variables:
        DOTENV: ".env.12-x-stable"
    ```

2. Create a new `.env.*` that corresponds to the version to release and specify the default configuration.

    ```text
    LM_PYTHON_VERSION '3.5'
    LM_REPORT_VERSION '2.0'
    ```

Please check the [Release Process documentation](https://gitlab.com/gitlab-org/security-products/release/blob/master/docs/release_process.md).

# Contributing

If you want to help, read the [contribution guidelines](CONTRIBUTING.md).
