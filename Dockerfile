ARG LICENSE_FINDER_VERSION=5.6.2

FROM licensefinder/license_finder:$LICENSE_FINDER_VERSION
MAINTAINER GitLab

ARG LICENSE_FINDER_VERSION
ENV LICENSE_FINDER_VERSION $LICENSE_FINDER_VERSION

# Install JDK 11
RUN cd /tmp && \
    wget --quiet --no-cookies https://github.com/AdoptOpenJDK/openjdk11-binaries/releases/download/jdk-11.0.2%2B9/OpenJDK11U-jdk_x64_linux_hotspot_11.0.2_9.tar.gz -O jdk-11.tgz && \
    tar xf /tmp/jdk-11.tgz && \
    mv jdk-11.0.2+9 /usr/lib/jvm/adoptopen_jdk11 && \
    rm /tmp/jdk-11.tgz
RUN npm install npm-install-peers

# Don't let Rubygem fail with the numerous projects using PG or MySQL,
# install realpath, includes for python3, and pip for python3
# Install .NET Core 2.2, 3.0 because it is not installed in the license_finder image (https://github.com/pivotal/LicenseFinder/pull/632).
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    libpq-dev libmysqlclient-dev realpath python3-dev python3-pip dotnet-sdk-2.2 dotnet-sdk-3.0 && \
    rm -rf /var/lib/apt/lists/*

# Don't load RVM automatically, it doesn't work with GitLab-CI
RUN mv /etc/profile.d/rvm.sh /rvm.sh

# Warning! Environment variable PIP_VERSION causes the upgrade of pip to fail.
ARG VERSION_OF_PIP=19.1.1
ENV VERSION_OF_PIP $VERSION_OF_PIP

ARG SETUPTOOLS_VERSION=41.0.1
ENV SETUPTOOLS_VERSION $SETUPTOOLS_VERSION

ARG LOCAL_PYPI_INDEX=/pypi
ENV LOCAL_PYPI_INDEX $LOCAL_PYPI_INDEX

# Install setuptools, and fetch a recent version pip to be installed later on
RUN pip3 install --disable-pip-version-check setuptools==$SETUPTOOLS_VERSION && \
    mkdir $LOCAL_PYPI_INDEX && \
    wget -q -O $LOCAL_PYPI_INDEX/pip-$VERSION_OF_PIP.tar.gz https://files.pythonhosted.org/packages/93/ab/f86b61bef7ab14909bd7ec3cd2178feb0a1c86d451bc9bccd5a1aedcde5f/pip-$VERSION_OF_PIP.tar.gz

# Version of Python, defaults to Python 3.5
ARG LM_PYTHON_VERSION=3.5
ENV LM_PYTHON_VERSION $LM_PYTHON_VERSION
ENV LM_REPORT_VERSION ${LM_REPORT_VERSION:-1}

COPY test /test
COPY run.sh /
COPY . /opt/license-management/
RUN bash -lc "source /rvm.sh && cd /opt/license-management && gem build *.gemspec && gem install *.gem"

ENTRYPOINT ["/run.sh"]
